# include "List.h"
# include "Node.h"
# include <stdlib.h>
# include <iostream>
using namespace std;
int menu()
{
	//显示通讯录功能菜单：
	cout << "功能菜单" << endl;
	cout << "1.新建联系人" << endl;
	cout << "2.删除联系人" << endl;
	cout << "3.浏览通讯录" << endl;
	cout << "4.退出通讯录" << endl;
	cout << "请输入:";
	int order = 0;
	cin >> order;
	return order;
}
void CreatPerson(List *pList)
{
	Node node;//插入到链表中必须有结点
	Person person;
	cout << "请输入姓名:";
	cin >> person.name;
	cout << "请输入电话:";
	cin >> person.phone;
	node.data = person;
	pList->ListInsertTail(&node);
}
void DeletePerson(List *pList)
{

	Node node;
	Person person;
	cout << "请输入要删除的人的姓名：";
	cin >> person.name;
	cout << "请输入要删除的人的电话：";
	cin >> person.phone;
	node.data = person;
	Node temp;
	int index = pList->LocateElem(&node);
	pList->ListDelete(index, &temp);
}
int main()
{
	int userOrder = 0;
	List *pList = new List();
	while (userOrder != 4)
	{
		userOrder = menu();
		switch (userOrder)
		{
		case 1:
			cout << "用户指令--->>新建联系人:" << endl;
			CreatPerson(pList);
			break;
		case 2:
			cout << "用户指令--->>删除联系人:" << endl; 
			DeletePerson(pList);
			break;
		case 3:
			cout << "用户指令--->>浏览通讯录:" << endl;
			pList->ListTraverse();
			break;
		case 4:
			cout << "用户指令--->>退出通讯录:" << endl;
			break;
		}
	}
	delete pList;
	pList = NULL;		
//使用指针的访问形式：
	//Node *node6 =new Node;
	//node6->data = 8;
	//pList->ListInsert(1, node6);	
	//system("pause");
	return 0;
}
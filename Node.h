# ifndef NODE_H
# define NODE_H
# include "Person.h"
# include <ostream>
# include <string>
using namespace std;
class Node
{
	
public:
	Person data;
	Node *next;
	void printNode();
};
# endif
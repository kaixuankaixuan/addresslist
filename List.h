# ifndef LIST_H
# define LISH_H
# include "Node.h"
class List
{
public:
	List();
	~List();
	void ClearList();
	bool ListEmpty();
	int listLength();
	bool GetElem(int i, Node *pNode);
	int LocateElem(Node *pNode);
	bool PriorElem(Node *pCurrentNode, Node *pPreNode);
	bool NextElem(Node *pCurrentNode, Node *pNextNode);
	void ListTraverse();
	bool ListInsert(int i, Node *pNode);
	bool ListDelete(int i, Node *pNode);
	bool ListInsertHead(Node *pNode);
	bool ListInsertTail(Node *pNode);
private:
	int m_iLength;
	Node *m_pList;

};
# endif
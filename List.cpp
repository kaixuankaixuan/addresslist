# include "List.h"
# include <stdlib.h>
# include <iostream>
using namespace std;
List::List()
{
	m_pList = new Node;
	//m_pList->data = 0;data是person类的话这么注释会有问题。
	m_pList->next = NULL;
	m_iLength = 0;

}
bool List::ListEmpty()
{
	if (!m_iLength)
		return true;
	else
		return false;
}
int List::listLength()
{
	return m_iLength;
}
void List::ClearList()
{
	Node *CurrentNode = m_pList->next;
	while (CurrentNode != NULL)
	{
		Node *temp = CurrentNode->next;
		delete CurrentNode;
		CurrentNode = temp;
	}
	m_pList->next = NULL;//方便下一次执行

}
List::~List()
{
	ClearList();
	delete m_pList;
	m_pList = NULL;
}
//插入头结点这事有点不太懂(修改了作者写的代码，秒懂)
bool List::ListInsertHead(Node *pNode)
{
	Node *temp = m_pList->next;//保留头结点指向的下一个指针
	Node *newNode = new Node;//要在堆中申请内存，储存pNode的数据，为啥？
	if (newNode == NULL)
	{
		return false;
	}
	newNode->data = pNode->data;
	m_pList->next = newNode;
	newNode->next = temp;
	m_iLength++;
	return true;

}
//实现在尾部插入结点
bool List::ListInsertTail(Node *pNode)
{
	Node *CurrentNode = m_pList;
	while (CurrentNode->next != NULL)
	{
		CurrentNode = CurrentNode->next;
	}
	Node *newNode = new Node;
	if (newNode == NULL)
	{
		return false;
	}
	newNode->data = pNode->data;//之前认为这个没必要，如果直接挂pNode当内存回收时会出现错误。
	newNode->next = NULL;
	CurrentNode->next = newNode;//为啥不能直接CurrentNode指向pNode呢？
	m_iLength++;
	return true;

}
bool List::ListInsert(int i, Node *pNode)
{
	if (i<0 || i>m_iLength)
	{
		return false;
	}
	Node *currentNode = m_pList;
	for (int k = 0; k < i; k++)
	{
		currentNode = currentNode->next;
	}
	Node *newNode = new Node;
	if (newNode == NULL)
	{
		return false;
	}
	newNode->data = pNode->data;
	newNode->next = currentNode->next;
	currentNode->next = newNode;
	return true;
}
//总觉着for循环中的范围是不对的，这样好像删除的是i+1的结点。
bool List::ListDelete(int i, Node *pNode)
{
	if (i < 0 || i >= m_iLength)
	{
		return false;
	}
	Node *currentNode = m_pList;
	Node *currentNodeBefor = NULL;
	for (int k = 0; k <= i; k++)
	{

		currentNodeBefor = currentNode;
		currentNode = currentNode->next;
	}
	currentNodeBefor->next = currentNode->next;
	pNode->data = currentNode->data;
	delete currentNode;
	currentNode = NULL;
	m_iLength--;
	return true;
}
bool List::GetElem(int i, Node *pNode)
{
	if (i < 0 || i >= m_iLength)
	{
		return false;
	}
	Node *currentNode = m_pList;
	Node *currentNodeBefor = NULL;
	for (int k = 0; k <= i; k++)
	{

		currentNodeBefor = currentNode;
		currentNode = currentNode->next;
	}
	pNode->data = currentNode->data;
	return true;

}
int List::LocateElem(Node *pNode)
{
	Node *currentNode = m_pList;
	int count = 0;
	while (currentNode->next != NULL)
	{
		currentNode = currentNode->next;
		if (currentNode->data == pNode->data)
		{
			return count;
		}
		count++;
	}
	return -1;
}
bool List::PriorElem(Node *pCurrentNode, Node *pPreNode)
{
	Node *currentNode = m_pList;
	Node *tempNode = NULL;
	while (currentNode->next != NULL)
	{
		tempNode = currentNode;
		currentNode = currentNode->next;
		if (currentNode->data == pCurrentNode->data)
		{
			if (tempNode == m_pList)
			{
				return false;
			}
			pPreNode->data = tempNode->data;
			return true;
		}

	}
	return false;
}
bool List::NextElem(Node *pCurrentNode, Node *pNextNode)
{
	Node *currentNode = m_pList;
	while (currentNode->next != NULL)
	{
		currentNode = currentNode->next;
		if (currentNode->data == pCurrentNode->data)
		{
			if (currentNode->next == NULL)
			{
				return false;
			}
			pNextNode->data = currentNode->next->data;
			return true;
		}

	}
	return false;
}
void List::ListTraverse()
{
	Node *currentNode = m_pList;
	while (currentNode->next!= NULL)
	{
		
		currentNode = currentNode->next;
		currentNode->printNode();
		
	}
}